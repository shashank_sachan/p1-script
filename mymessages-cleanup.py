# Author
# Shashank Sachan
# Date : Mar 12th, 2017
# Usage
# Delete message & meta data from alerts table.
# We can set follwing 3 variable
# older_than_x_day : Delete message older than x days. Default is set to 90
# delete_expired : Delete expired message or not. Default is set to True
# delete_from_alert_messages: Delete message from alert_messages table or not. Default is set to False
# RUN: python mymessages-cleanup.py
# Ubuntu: apt-get install python-dev libmysqlclient-dev; pip install MySQL-python
# Centos: yum install MySQL-python

import MySQLdb as mdb
from datetime import datetime
import sys
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('/apps/logs/tomcat/mymessages-cleanup.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class ConnectionManager(object):
    def __init__(self):
        self.username  = 'root'
        self.password  = 'admin2008'
        self.campuseai = 'campuseai'
        self.server    = '127.0.0.1'
    
    def connect(self):
        try:
            conn = mdb.connect(self.server, self.username, self.password, self.campuseai)
            logger.info('DB connection established successfully.')
            return conn
        except Exception, e:
            logger.error('DB Connection Failed : {0}'.format(e))
            sys.exit()
    
    def disconnect(self, conn):
        conn.close()
        logger.info('DB Connection closed')



class MessageManager(object):
    def __init__(self, conn, older_than_days=90, delete_from_alert_messages=False):
        self.con = conn
        self.older_than_days = older_than_days
        self.delete_from_alert_messages = delete_from_alert_messages

    def get_ids_from_alerts_message(self):
        message_ids = []
        query = "select id from alerts_message WHERE date < DATE_SUB(NOW(), INTERVAL {0} DAY)".format(self.older_than_days)
        cur = self.con.cursor(mdb.cursors.DictCursor)
        logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            if row is None: continue
            message_ids.append(row['id'])
        logger.info("Message count older than {0} days : {1}".format(self.older_than_days, len(message_ids)))
        return message_ids

    def get_ids_from_expired(self):
        current_date = datetime.now()
        message_ids = []
        query = "select message_id, value from alerts_message_attributes where name='expires'"
        cur = self.con.cursor(mdb.cursors.DictCursor)
        logger.info('Executing Query : {0}'.format(query))
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            if row is None: continue
            date = datetime.strptime(row['value'].split()[0], '%Y-%m-%d')
            if date < current_date:
                message_ids.append(row['message_id'])
        logger.info("Expired message count : {0}".format(len(message_ids)))
        return message_ids

    def delete_message(self, message_ids):

        for message_id in message_ids:
            logger.info('Deleting {0}'.format(message_id))

            delcur = self.con.cursor()

            alert_ids = self.alerts_alert(message_id)
            if alert_ids is not None: 
                for alert_id in alert_ids: delcur.execute(self.alerts_deliverystatus(alert_id))
            delcur.execute(self.alerts_alert_del(message_id))

            attr_ids = self.alerts_message_attributes(message_id)
            for attr_id in attr_ids: delcur.execute(self.alerts_message_attributes_del(attr_id))
            
            delcur.execute(self.alerts_recipients(message_id))

            if self.delete_from_alert_messages: delcur.execute(self.alerts_message_del(message_id))
            self.con.commit()

    def alerts_message_del(self, message_id):
        logger.info('Deleting message ID {0} from alerts_message'.format(message_id))
        return "delete from alerts_message where id={0}".format(message_id)
    
    def alerts_alert(self, message_id):
        alert_ids = []
        query = "select id from alerts_alert where message_id={0}".format(message_id)
        cur = self.con.cursor(mdb.cursors.DictCursor)
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            alert_ids.append(row['id'])
        if len(alert_ids) == 0: return None
        else: return alert_ids

    def alerts_alert_del(self, message_id):
        logger.info('Deleting message ID {0} from alerts_alert'.format(message_id))
        return "delete from alerts_alert where message_id={0}".format(message_id)

    def alerts_recipients(self, message_id):
        logger.info('Deleting message ID {0} from alerts_recipients'.format(message_id))
        return "delete from alerts_recipients where message_id={0}".format(message_id)
    
    def alerts_deliverystatus(self, alert_id):
        logger.info('Deleting message ID {0} from alerts_deliverystatus'.format(alert_id))
        return "delete from alerts_deliverystatus where alert_id={0}".format(alert_id)
    
    def alerts_message_attributes(self, message_id):
        attr_ids = []
        query = "select id from alerts_message_attributes where message_id={0}".format(message_id)
        cur = self.con.cursor(mdb.cursors.DictCursor)
        cur.execute(query)
        for i in range(cur.rowcount):
            row = cur.fetchone()
            attr_ids.append(row['id'])
        return attr_ids
    
    def alerts_message_attributes_del(self, attr_id):
        logger.info('Deleting message ID {0} from alerts_message_attributes'.format(attr_id))
        return "delete from alerts_message_attributes where id={0}".format(attr_id)


try:
    #Variables
    older_than_x_day = 90
    delete_expired = True
    delete_from_alert_messages = False

    #DB connection Manager
    db = ConnectionManager()
    conn = db.connect()

    #MyMessages Delete Manager
    messageObj  = MessageManager(conn, older_than_x_day, delete_from_alert_messages)
    g_id_am = messageObj.get_ids_from_alerts_message()
    if delete_expired: 
        g_id_ex = messageObj.get_ids_from_expired()
        uniq_ids = list(set(g_id_am) | set(g_id_ex))
    else: 
        uniq_ids = g_id_am
    logger.info(uniq_ids)
    messageObj.delete_message(uniq_ids)

except Exception, e:
    logger.error(e)
finally:
    db.disconnect(conn)