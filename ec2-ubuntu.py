#Title - ec2-ubuntu.py
#Description - Run this script on P1 for capturing all the logs (EC2 Ubuntu Environment)
#Author - Shashank Sachan
#Date - April 26th, 2017
#Version - 0.1
#Usage - python ec2-ubuntu.py
#Download - https://gitlab.com/shashank_sachan/p1-script/raw/master/ec2-ubuntu.py
"""
1. Always check disk space on the server before running the script.
2. If log size is more than 1 GB then script will skip that log. Take that log backup manually if required.
3. If heap info (jmap -heap process_id) is taking more than 10 seconds then script will terminate that thread. You can see the same in the running log (In Yellow color). In this case try to take the heap info manually.
4. if 'sysstat' package is not installed then iowait and sar info will not be included.
On Centos: yum install sysstat
On Ubuntu: apt-get install sysstat
On Ubuntu, you also need to set to ENABLED="true" in "/etc/default/sysstat" file.
"""

from datetime import datetime
from glob import glob
import subprocess
import threading
import shutil
import apt
import os
import socket


class settings(object):
    backup_location = '/apps/P1_backup'
    jmap = '/apps/jdk/current/bin/jmap'
    mysql = '/apps/mysql/current/bin/mysql'
    
    mysql_user = 'root'
    mysql_pass = 'admin2008'
    mysql_server = 'dbhost'
    
    log_tomcat = '/apps/logs/tomcat'
    log_portlet = '/apps/logs/portlets'
    log_mysql = '/apps/logs/mysql'
    log_mysql_name = ['error.log','slow-query.log']
    log_cas = '/apps/logs/tomcat/cas.log*'

def is_installed_sysstat():
    package = 'sysstat'
    cache = apt.Cache()
    if cache[package].is_installed:
        return True
    else:
        return False
    
def is_installed_mytop():
    package = 'mytop'
    cache = apt.Cache()
    if cache[package].is_installed:
        return True
    else:
        return False

def install_mytop():
    try:
        print 'Installing mytop package'
        package = 'mytop'
        cache = apt.Cache()
        pkg = cache[package]
        pkg.mark_install()
        cache.commit()
        return True
    except Exception, e:
        return False

class print_write(object):
    def __init__(self):
        self.INFO = '\033[96m'
        self.SUCCESS = '\033[92m'
        self.ERROR = '\033[91m'
        self.WARN = '\033[93m'
        self.END = '\033[0m'
        
    def info(self, msg):
        return '{0}{1}{2} '.format(self.INFO, msg, self.END)
    
    def success(self, msg):
        return '{0}{1}{2} '.format(self.SUCCESS, msg, self.END)

    def error(self, msg):
        return '{0}{1}{2} '.format(self.ERROR, msg, self.END)

    def warn(self, msg):
        return '{0}{1}{2} '.format(self.WARN, msg, self.END)

###################
pw = print_write()
###################

class MyDateTime(object):
    def date(self):
        date = datetime.now().date()
        date = date.strftime('%Y-%m-%d')
        return date
    def time(self, format=None):
        time = datetime.now().time()
        if format == 'colon': time = time.strftime('%H:%M:%S')
        else: time = time.strftime('%H.%M.%S')
        return time
    def fileModTime(self, log):
        mt = os.path.getmtime(log)
        dt = datetime.fromtimestamp(mt)
        date = dt.strftime('%Y-%m-%d')
        return date
    
class NetworkUtility(object):
    def __init__(self):
        self.timeout = 5
        
    def telnet(self, ip=None, port=None):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(self.timeout)
            res = s.connect_ex((ip, port))
            s.close()
            if res == 0:
                return True, res
            else:
                return False, res
        except Exception,e:
            return False, e
   
class GetPid(object):
    def __init__(self):
        self.utility = 'pgrep'
        self.java_proc = '{0} -u mycampus'.format(self.utility)
        self.mysql_proc = '{0} -u mysql'.format(self.utility)
    
    def _get_java_pid(self):
        p = subprocess.Popen(self.java_proc, shell=True, stdout=subprocess.PIPE)
        output, error = p.communicate()
        if error is not None:
            return False, error
        elif output.strip().__len__() == 0:
            return False, 'LookingGlass service is not running on the server.'
        else:
            pid = int(output.strip())
            return True, pid

    def _get_mysql_pid(self):
        p = subprocess.Popen(self.mysql_proc, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = p.communicate()
        if error.__len__() == 0: error = None
        if error is not None:
            return False, error
        elif output.strip().__len__() == 0:
            return False, 'MySQL service is not running on the server.'
        else:
            pid = int(output.strip())
            return True, pid

class MysqlUtility(object):
    def __init__(self, user=settings.mysql_user, passwd=settings.mysql_pass, server=settings.mysql_server, mysqld=settings.mysql):
        self.user = user
        self.passwd = passwd
        self.server = server
        self.mysqld = mysqld
        self.mysql = '{0} -u{1} -p{2} -h{3}'.format(self.mysqld, self.user, self.passwd, self.server)
        
    def _get_databases(self):
        query = "{0} -e 'show databases'".format(self.mysql)
        output, err = self.execute(query)
        if err is None:
            return True, output.split()[1:]
        else:
            return False, err
        
    def _get_tables(self, dbname):
        query = "{0} {1} -e 'show tables'".format(self.mysql, dbname)
        output, err = self.execute(query)
        if err is None:
            return True, output.split()[1:]
        else:
            return False, err

    def _get_table_row_count(self, dbname, table):
        query = "{0} {1} -e 'select count(*) from {2}'".format(self.mysql, dbname, table)
        output, err = self.execute(query)
        if err is None:
            return True, output.split()[-1]
        else:
            return False, err
        
    def execute(self, cmd):
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        if err.__len__() == 0: err = None
        return output, err
    
class MysqlInformation(object):
    def __init__(self, P1_location):
        self.mysql_user = settings.mysql_user
        self.mysql_pass = settings.mysql_pass
        self.mysql_server = settings.mysql_server
        
        self.backup_location = P1_location
        self.mysql_thread_log = '{0}/{1}'.format(self.backup_location, 'mysql_threads_from_top')
        self.mysql_openfile_desc_log = "{0}/{1}".format(self.backup_location, 'mysql_openfile_desc')
        self.mysql_show_proclist_from_cmd_log = "{0}/{1}".format(self.backup_location, 'mysql_show_proclist_from_cmd')
        self.mysql_show_proclist_from_schema_log = "{0}/{1}".format(self.backup_location, 'mysql_show_proclist_from_schema')
        self.cas_schema_log = "{0}/{1}".format(self.backup_location, 'mysql_cas_schema')
        self.mysql_mytop_log = "{0}/{1}".format(self.backup_location, 'mysql_mytop')
        self.mysql_erpsis_log = "{0}/{1}".format(self.backup_location, 'mysql_erpsis')
    
        
    def mysql_threads(self):
        mysql_thread_cmd = 'top -d0.2 -bn2 -u mysql -H'
        output, err = self.execute(mysql_thread_cmd)
        if err is None:
            status, resp = self.write(mysql_thread_cmd, output, self.mysql_thread_log)
            return status, resp
        else:
            return False, err
        
    def mysql_openfile_desc(self, mysql_pid):
        mysql_openfile_desc_cmd = 'ls -al /proc/{0}/fd/| wc -l'.format(mysql_pid)
        output, err = self.execute(mysql_openfile_desc_cmd)
        if err is None:
            status, resp = self.write(mysql_openfile_desc_cmd, output, self.mysql_openfile_desc_log)
            return status, resp
        else:
            return False, err
    
    def mysql_show_proclist_from_cmd(self):
        query = 'show full processlist;'
        mysql_show_proclist_cmd = "mysql -u{0} -p{1} -e '{2}'".format(self.mysql_user, self.mysql_pass, query)
        output, err = self.execute(mysql_show_proclist_cmd)
        if err is None:
            status, resp = self.write(mysql_show_proclist_cmd, output, self.mysql_show_proclist_from_cmd_log)
            return status, resp
        else:
            return False, err
        
    def mysql_show_proclist_from_schema(self):
        query = 'select * from information_schema.PROCESSLIST;'
        mysql_show_proclist_schema = "mysql -u{0} -p{1} -e '{2}'".format(self.mysql_user, self.mysql_pass, query)
        output, err = self.execute(mysql_show_proclist_schema)
        if err is None:
            status, resp = self.write(mysql_show_proclist_schema, output, self.mysql_show_proclist_from_schema_log)
            return status, resp
        else:
            return False, err
        
    def mysql_verify_cas_schema(self):
        mysqlUtilityObj = MysqlUtility()
        status, resp = mysqlUtilityObj._get_databases()
        if status:
            if 'cas' in resp:
                status, resp = mysqlUtilityObj._get_tables('cas')
                if status:
                    if resp.__len__() == 0:
                        return False, 'CAS schema is present but no tables'
                    else:
                        for table in resp:
                            status, response = mysqlUtilityObj._get_table_row_count('cas', table)
                            if status:
                                status, resp = self.write(table, response, self.cas_schema_log)
                        return True, self.cas_schema_log
                else:
                    return status, resp
            else:
                return False, 'CAS schema is not present'
        else:
            return False, resp
                            
    def mysql_run_mytop(self):
        if not is_installed_mytop(): install_mytop()
        if is_installed_mytop():
            mysql_mytop_cmd = 'mytop -u{0} -p{1} -dlportal -h{2} H -b'.format(self.mysql_user, self.mysql_pass, self.mysql_server)
            output, err = self.execute(mysql_mytop_cmd)
            if err is None:
                status, resp = self.write(mysql_mytop_cmd, output, self.mysql_mytop_log)
                return status, resp
            else:
                return False, err
        else:
            return False, "'mytop' package is not installed. Cannot retrieve mytop report from mysql."
    
    def mysql_erpsis_connectivity(self):
        query = "select PC_VALUE from campuseai.portletconfig where pc_key='ds.connectString' and PC_NAMESPACE='org.campuseai.erpsis.banner-banner';"
        mysql_erpsis_cmd = 'mysql -u{0} -p{1} -e "{2}"'.format(self.mysql_user, self.mysql_pass, query)
        output, err = self.execute(mysql_erpsis_cmd)
        if err is None:
            status, resp = self.write(mysql_erpsis_cmd, output, self.mysql_erpsis_log)
            return status, resp
        else:
            return False, err
    
    def execute(self, cmd):
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        if err.__len__() == 0: err = None
        return output, err
    
    def write(self, cmd=None, logs=None, log_file=None):
        try:
            with open(log_file, 'a') as f:
                f.write('\n{0}\n'.format(cmd))
                f.write('*'*50 + '\n')
                f.write(logs)
                f.write('\n' + '*'*50 + '\n')
            return True, log_file
        except Exception, e:
            return False, e
         
class MycampusInformation(object):
    def __init__(self, P1_location):
        self.timeout = 10
        self.backup_location = P1_location
        self.java_thread_log = '{0}/{1}'.format(self.backup_location, 'mycampus_threads_from_top')
        self.java_openfile_desc_log = "{0}/{1}".format(self.backup_location, 'mycampus_openfile_desc')
        self.heap_info_log = "{0}/{1}".format(self.backup_location, 'mycampus_heap_info_log')
        self.ulimit_config_log = '{0}/{1}'.format(self.backup_location, 'mycampus_ulimit_config')
        self.ulimit_runtime_log = '{0}/{1}'.format(self.backup_location, 'mycampus_ulimit_runtime')
        self.histo_log =  '{0}/{1}'.format(self.backup_location, 'mycampus_histo')
        
    def mycampus_threads(self):
        java_thread_cmd = 'top -d0.2 -bn5 -u mycampus -H'
        output, err = self.execute(java_thread_cmd)
        if err is None:
            status, resp = self.write(java_thread_cmd, output, self.java_thread_log)
            return status, resp
        else:
            self.write(java_thread_cmd, output, self.java_thread_log)
            return False, err
        
    def mycampus_openfile_desc(self, java_pid):
        java_openfile_desc_cmd = 'ls -al /proc/{0}/fd/| wc -l'.format(java_pid)
        output, err = self.execute(java_openfile_desc_cmd)
        if err is None:
            status, resp = self.write(java_openfile_desc_cmd, output, self.java_openfile_desc_log)
            return status, resp
        else:
            return False, err
    
    def mycampus_heap_info(self, java_pid):
        heap_info_cmd = '{0} -heap {1}'.format(settings.jmap, java_pid)
        def target():
            self.heapprocess = subprocess.Popen(heap_info_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, err = self.heapprocess.communicate()
            self.write(heap_info_cmd, output, self.heap_info_log)
        
        thread = threading.Thread(target=target)
        thread.start()
        thread.join(self.timeout)
        
        if thread.is_alive():
            print pw.warn('[{0} {1}] [Module 11] [WARNING] Heap info thread is taking more than {2} seconds. Terminating heap info thread....'.format(MyDateTime().date(), MyDateTime().time(format='colon'), self.timeout))
            self.heapprocess.terminate()
            thread.join()
            with open(self.heap_info_log) as f:
                if 'Old Generation' in f.read():
                    return True, self.heap_info_log
                else:       
                    return False, "Check the heap info manually (jmap -heap <JAVA_PID>) and save the output in '{0}' file".format(self.heap_info_log)
        return True, self.heap_info_log
    
    def mycampus_thread_dump(self, java_pid):
        thread_dump = 'kill -3 {0}'.format(java_pid)
        output, err = self.execute(thread_dump)
        if err is None:
            return True, 'cataline.out'
        else:
            return False, err
        
    def mycampus_ulimit_config(self):
        ulimit_config_cmd = 'cat /etc/security/limits.conf | grep ^mycampus'
        output, err = self.execute(ulimit_config_cmd)
        if err is None:
            status, resp = self.write(ulimit_config_cmd, output, self.ulimit_config_log)
            return status, resp
        else:
            return False, err
    
    def mycampus_ulimit_runtime(self, java_pid):
        ulimit_runtime_cmd = 'cat /proc/{0}/limits'.format(java_pid)
        output, err = self.execute(ulimit_runtime_cmd)
        if err is None:
            status, resp = self.write(ulimit_runtime_cmd, output, self.ulimit_runtime_log)
            return status, resp
        else:
            return False, err
    
    def mycampus_histo(self, java_pid):
        histo_cmd = "su - mycampus -c 'jmap -histo {0}'".format(java_pid)
        output, err = self.execute(histo_cmd)
        if err is None:
            status, resp = self.write(histo_cmd, output, self.histo_log)
            return status, resp
        else:
            return False, err
    
    def execute(self, cmd):
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        if err.__len__() == 0: err = None
        return output, err
    
    def write(self, cmd=None, logs=None, log_file=None):
        try:
            with open(log_file, 'a') as f:
                f.write('{0}\n'.format(cmd))
                f.write('*'*50 + '\n')
                f.write(logs)
                f.write('\n' + '*'*50)
            return True, log_file
        except Exception, e:
            return False, e
        
class ServerInformation(object):
    def __init__(self, P1_location):
        self.backup_location = P1_location
        self.default_top_log = '{0}/{1}'.format(self.backup_location, 'server_top_info')
        self.top_cpu_log = '{0}/{1}'.format(self.backup_location, 'server_top_cpu_info')
        self.network_conn_log = '{0}/{1}'.format(self.backup_location, 'server_network_connection')
        self.primary_mem_log = '{0}/{1}'.format(self.backup_location, 'server_primary_memory')
        self.secondary_mem_log = '{0}/{1}'.format(self.backup_location, 'server_secondary_memory')
        self.uptime_log = '{0}/{1}'.format(self.backup_location, 'server_uptime')
        self.iowait_log = '{0}/{1}'.format(self.backup_location, 'server_iowait')
        self.sar_log = '{0}/{1}'.format(self.backup_location, 'server_sar_report')
        
    def server_default_top(self):
        default_top_cmd = 'top -d0.2 -bn2'
        output, err = self.execute(default_top_cmd)
        if err is None:
            status, resp = self.write(default_top_cmd, output, self.default_top_log)
            return status, resp
        else:
            return False, err
        
    def server_cpu_top(self):
        cpu_top_cmd = 'top -b -d1 -n5| grep Cpu'
        output, err = self.execute(cpu_top_cmd)
        if err is None:
            status, resp = self.write(cpu_top_cmd, output, self.top_cpu_log)
            return status, resp
        else:
            return False, err
    
    def server_network_connection(self):
        network_conn_cmd = 'netstat -anp'
        output, err = self.execute(network_conn_cmd)
        if err is None:
            status, resp = self.write(network_conn_cmd, output, self.network_conn_log)
            return status, resp
        else:
            return False, err
        
    def server_primary_memory(self):
        primary_mem_cmd = 'free -m'
        output, err = self.execute(primary_mem_cmd)
        if err is None:
            status, resp = self.write(primary_mem_cmd, output, self.primary_mem_log)
            return status, resp
        else:
            return False, err
    
    def server_secondary_memory(self):
        secondary_mem_cmd = 'df -h'
        output, err = self.execute(secondary_mem_cmd)
        if err is None:
            status, resp = self.write(secondary_mem_cmd, output, self.secondary_mem_log)
            return status, resp
        else:
            return False, err
    
    def server_uptime_loadavg(self):
        uptime_cmd = 'uptime'
        output, err = self.execute(uptime_cmd)
        if err is None:
            status, resp = self.write(uptime_cmd, output, self.uptime_log)
            return status, resp
        else:
            return False, err
    
    def server_iowait(self):
        if is_installed_sysstat():
            iowait_cmd = 'iostat -c 2 -N 5'
            output, err = self.execute(iowait_cmd)
            if err is None:
                status, resp = self.write(iowait_cmd, output, self.iowait_log)
                return status, resp
            else:
                return False, err
        else:
            return False, "'sysstat' package is not installed. Cannot retrieve iowait report."
    
    def server_sar_report(self):
        if is_installed_sysstat():
            sar_cmd = 'sar -A'
            output, err = self.execute(sar_cmd)
            if err is None:
                status, resp = self.write(sar_cmd, output, self.sar_log)
                return status, resp
            else:
                return False, err
        else:
            return False, "'sysstat' package is not installed. Cannot retrieve sar report."
    
    def execute(self, cmd):
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        if err.__len__() == 0: err = None
        return output, err
    
    def write(self, cmd=None, logs=None, log_file=None):
        try:
            with open(log_file, 'a') as f:
                f.write('{0}\n'.format(cmd))
                f.write('*'*50 + '\n')
                f.write(logs)
                f.write('\n' + '*'*50)
            return True, log_file
        except Exception, e:
            return False, e

class MyLogsBackup(object):
    def __init__(self, P1_location):
        self.backup_location = P1_location
        self.fileModObj = MyDateTime()
        self.currentDate = self.fileModObj.date()
        self.threshold_size = 1024
        
    def myCasLogs(self):
        src_loc = settings.log_cas
        dst_loc = self.backup_location
        for log in glob(src_loc):
            log_size = os.path.getsize(log)/(1024*1024.0)
            print '[{0} {1}] [Module 23] [Copying] {2} ({3:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), log, log_size)
            shutil.copy2(log, dst_loc)
        return True, src_loc
            
    def myTomcatLogs(self):
        src_loc = settings.log_tomcat
        dst_loc = self.backup_location
        if os.path.exists(src_loc):
            for log in os.listdir(src_loc):
                log = os.path.join(src_loc, log)
                if self.currentDate == self.fileModObj.fileModTime(log):
                    log_size = os.path.getsize(log)/(1024*1024.0)
                    if int(log_size) > self.threshold_size:
                        print pw.warn('[{0} {1}] [Module 24] [WARNING] Log size in more than {2} MB. Copy this log manually. {3} ({4:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), self.threshold_size, log, log_size))
                    else:        
                        print '[{0} {1}] [Module 24] [Copying] {2} ({3:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), log, log_size)
                        shutil.copy2(log, dst_loc)
            return True, src_loc
        else:
            return False, "'{0}' doesn't exist".format(src_loc)
    
    def myPortletLogs(self):
        src_loc = settings.log_portlet
        dst_loc = self.backup_location
        if os.path.exists(src_loc):
            for log in os.listdir(src_loc):
                log = os.path.join(src_loc, log)
                if self.currentDate == self.fileModObj.fileModTime(log):
                    log_size = os.path.getsize(log)/(1024*1024.0)
                    if int(log_size) > self.threshold_size:
                        print pw.warn('[{0} {1}] [Module 25] [WARNING] Log size in more than {2} MB. Copy this log manually. {3} ({4:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), self.threshold_size, log, log_size))
                    else:   
                        print '[{0} {1}] [Module 25] [Copying] {2} ({3:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), log, log_size)
                    shutil.copy2(log, dst_loc)
            return True, src_loc
        else:
            return False, "'{0}' doesn't exist".format(src_loc)
    
    def myMysqlLogs(self):
        src_loc = settings.log_mysql
        dst_loc = self.backup_location
        log_names = settings.log_mysql_name
        
        if os.path.exists(src_loc):
            for log in log_names:
                log = os.path.join(src_loc, log)
                log_size = os.path.getsize(log)/(1024*1024.0)
                if int(log_size) > self.threshold_size:
                    print pw.warn('[%s %s] [Module 26] [WARNING] Log size in more than %s MB. Copy this log manually. %s (%.2f MB)...' % (MyDateTime().date(), MyDateTime().time(format='colon'), self.threshold_size, log, log_size))
                else:
                    print '[{0} {1}] [Module 26] [Copying] {2} ({3:.2f} MB)...'.format(MyDateTime().date(), MyDateTime().time(format='colon'), log, log_size)
                    shutil.copy2(log, dst_loc)
            return True, src_loc
        else:
            return False, "'{0}' doesn't exist".format(src_loc)


datetimeObj = MyDateTime()
date = datetimeObj.date()
time = datetimeObj.time()

if not os.path.exists(settings.backup_location):
    print '[{0} {1}] Backup directory not present'.format(MyDateTime().date(), MyDateTime().time(format='colon'))
    print '[{0} {1}] Create backup directory - {2}'.format(MyDateTime().date(), MyDateTime().time(format='colon'), settings.backup_location)
    os.mkdir(settings.backup_location)
else:
    print '[{0} {1}] Backup directory present - {2}'.format(MyDateTime().date(), MyDateTime().time(format='colon'), settings.backup_location)


hostname = socket.gethostname()
P1_location = 'P1-{0}-{1}.{2}'.format(hostname, date, time)
P1_location = '{0}/{1}'.format(settings.backup_location, P1_location)
print '[{0} {1}] Creating P1 directory - {2}'.format(MyDateTime().date(), MyDateTime().time(format='colon'), P1_location)
os.mkdir(P1_location)


serverObj = ServerInformation(P1_location)

print "[{0} {1}] [Module 1] [PROCESS] Retrieving top information...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_default_top()
if status: print pw.success("[{0} {1}] [Module 1] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 1] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 2] [PROCESS] Retrieving cpu utilization information...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_cpu_top()
if status: print pw.success("[{0} {1}] [Module 2] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}]  [Module 2] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 3] [PROCESS] Retrieving network connection...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_network_connection()
if status: print pw.success("[{0} {1}] [Module 3] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 3] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 4] [PROCESS] Retrieving primary memory info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_primary_memory()
if status: print pw.success("[{0} {1}] [Module 3] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 4] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 5] [PROCESS] Retrieving seconday memory info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_secondary_memory()
if status: print pw.success("[{0} {1}] [Module 5] [PROCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 5] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 6] [PROCESS] Retrieving uptime/loadavg info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_uptime_loadavg()
if status: print pw.success("[{0} {1}] [Module 6] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 6] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 7] [PROCESS] Retrieving iowait info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_iowait()
if status: print pw.success("[{0} {1}] [Module 7] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 7] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 8] [PROCESS] Retrieving sar report...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = serverObj.server_sar_report()
if status: print pw.success("[{0} {1}] [Module 8] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 8] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))


pidObj = GetPid()

status, resp = pidObj._get_java_pid()
if status:
    java_pid = resp
    mycampusObj = MycampusInformation(P1_location)
    
    print "[{0} {1}] [Module 9] [PROCESS] Retrieving mycampus thread count...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_threads()
    if status: print pw.success("[{0} {1}] [Module 9] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 9] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 10] [PROCESS] Retrieving mycampus openfile desc count...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_openfile_desc(java_pid)
    if status: print pw.success("[{0} {1}] [Module 10] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 10] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 11] [PROCESS] Retrieving mycampus heap info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_heap_info(java_pid)
    if status: print pw.success("[{0} {1}] [Module 11] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 11] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 12] [PROCESS] Taking mycampus thread dump...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_thread_dump(java_pid)
    if status: print pw.success("[{0} {1}] [Module 12] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 12] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 13] [PROCESS] Retrieving mycampus ulimit config...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_ulimit_config()
    if status: print pw.success("[{0} {1}] [Module 13] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 13] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 14] [PROCESS] Retrieving mycampus process ulimit...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_ulimit_runtime(java_pid)
    if status: print pw.success("[{0} {1}] [Module 14] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 14] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 15] [PROCESS] Retrieving mycampus histo...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp =  mycampusObj.mycampus_histo(java_pid)
    if status: print pw.success("[{0} {1}] [Module 15] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 15] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else:
    print pw.error("[{0} {1}] [Module 9,10,11,12,13,14,15] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

status, resp = pidObj._get_mysql_pid()
if status:
    mysql_pid = resp
    mysqlObj = MysqlInformation(P1_location)
    
    print "[{0} {1}] [Module 16] [PROCESS] Retrieving mysql thread count...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_threads()
    if status: print pw.success("[{0} {1}] [Module 16] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 16] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 17] [PROCESS] Retrieving mysql openfile desc count...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_openfile_desc(mysql_pid)
    if status: print pw.success("[{0} {1}] [Module 17] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 17] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 18] [PROCESS] Retrieving mysql processlist from cmd...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_show_proclist_from_cmd()
    if status: print pw.success("[{0} {1}] [Module 18] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 18] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 19] [PROCESS] Retrieving mysql processlist from schema...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_show_proclist_from_schema()
    if status: print pw.success("[{0} {1}] [Module 19] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 19] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 20] [PROCESS] Retrieving mysql CAS schema info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_verify_cas_schema()
    if status: print pw.success("[{0} {1}] [Module 20] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.info("[{0} {1}] [Module 20] [INFO] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    print "[{0} {1}] [Module 21] [PROCESS] Retrieving mysql info using mytop...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_run_mytop()
    if status: print pw.success("[{0} {1}] [Module 12] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 21] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

    print "[{0} {1}] [Module 22] [PROCESS] Retrieving mysql erpsis info...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
    status, resp = mysqlObj.mysql_erpsis_connectivity()
    if status: print pw.success("[{0} {1}] [Module 22] [SUCCESS] Successfully saved in {2}.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    else: print pw.error("[{0} {1}] [Module 22] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

else:
    print pw.warn("[{0} {1}] [Module 16,17,18,19,20,21,22] [WARNING] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
    
    
    
logsObj = MyLogsBackup(P1_location)

print "[{0} {1}] [Module 23] [PROCESS] Taking backup of CAS logs...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = logsObj.myCasLogs()
if status: print pw.success("[{0} {1}] [Module 23] [SUCCESS] Successfully saved {2} logs.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 23] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 24] [PROCESS] Taking backup of TOMCAT logs...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = logsObj.myTomcatLogs()
if status: print pw.success("[{0} {1}] [Module 24] [SUCCESS] Successfully {2} logs.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 24] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 25] [PROCESS] Taking backup of PORTLET logs...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = logsObj.myPortletLogs()
if status: print pw.success("[{0} {1}] [Module 25] [SUCCESS] Successfully saved {2} logs.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 25] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))

print "[{0} {1}] [Module 26] [PROCESS] Taking backup of MySQL logs...".format(MyDateTime().date(), MyDateTime().time(format='colon'))
status, resp = logsObj.myMysqlLogs()
if status: print pw.success("[{0} {1}] [Module 26] [SUCCESS] Successfully saved {2} logs.".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))
else: print pw.error("[{0} {1}] [Module 26] [ERROR] {2}".format(MyDateTime().date(), MyDateTime().time(format='colon'), resp))